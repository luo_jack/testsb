package com.accp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@SpringBootApplication
//@MapperScan("com.accp.mapper")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
//    @Bean
//    public RedisTemplate<String, Object> jdkRedisTemplate(RedisConnectionFactory factory){
//    	RedisTemplate<String, Object> redis=new RedisTemplate<>();
//    	redis.setConnectionFactory(factory);
//    	redis.setKeySerializer(RedisSerializer.string());
//    	redis.setValueSerializer(RedisSerializer.json());
//    	return redis;
//    }

}